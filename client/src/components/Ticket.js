import React, {Component} from 'react'
import TicketDescription from './Ticket/TicketDescription'
import TicketKey from './Ticket/TicketKey'
import TicketType from './Ticket/IssueType'

export default class Ticket extends Component {
  render() {
    return <div className="ticket">
      <h1>{this.props.summary}</h1>

      <div className="details container">
        <div className="col-md-6 col-sm-6">
          <TicketKey issueKey={this.props.issueKey} />
          <TicketType issueType={this.props.issueType} />
          <div className="row status-row">
            <div className="col-md-6">
              <span className="text-muted">Status:</span>
            </div>
            <div className="col-md-6">
              <span>{this.props.status.name}</span>
            </div>
          </div> 
        </div>

        <div className="col-md-6 col-sm-6">
          <div className="row reporter-row">
            <div className="col-md-6">
              <span className="text-muted">Reporter:</span>
            </div>
            <div className="col-md-6">
              <span>{this.props.reporter}</span>
            </div>
          </div> 

          <div className="row assignee-row">
            <div className="col-md-6">
              <span className="text-muted">Assignee:</span>
            </div>
            <div className="col-md-6">
              <span>{this.props.assignee}</span>
            </div>
          </div> 
        </div>
      </div>

      <div className="container">
        <TicketDescription description={this.props.description} />
      </div>
    </div>
  }
}
