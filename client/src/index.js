import React from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css'

import App from './components/App'

const div = document.getElementById('app')

ReactDOM.render(
  <App />,
  div
)
