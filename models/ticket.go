package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// StatusType is an enum representing one of Open, InProgress, or Done
type StatusType int

const (
	// Open is part of the StatusType enum
	Open StatusType = iota
	// InProgress is part of the StatusType enum
	InProgress
	// Done is part of the StatusType enum
	Done
)

// TODO: put this in the database so it can be extended.
var IssueTypes = [...]string{
	"bug",
	"enhancement",
	"feature",
}

// Ticket represents an issue / ticket.
type Ticket struct {
	ID          uint      `json:"id" gorm:"primary_key"`
	ProjectID   uint      `json:"-"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
	Key         string    `json:"issueKey" gorm:"primary_key"`
	Summary     string    `json:"summary"`
	Description string    `json:"description"`
	Type        string    `json:"issueType"`
	Reporter    uint      `json:"reporter"`
	Assignee    uint      `json:"assignee"`
	Comments    []Comment `json:"comments,omitempty"`
	Status      Status    `json:"status"`
}

// Update takes another ticket and will update all the fields of this ticket
// with the fields of the ticket given which are not empty.
func (t *Ticket) Update(ot *Ticket) *Ticket {
	if ot.Key != "" {
		t.Key = ot.Key
	}

	if ot.Summary != "" {
		t.Summary = ot.Summary
	}

	if ot.Description != "" {
		t.Description = ot.Description
	}

	return t
}

// Comment is a comment on an issue / ticket.
type Comment struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	TicketID  uint      `json:"-"`
	Author    User      `json:"author"`
	Body      string    `json:"body"`
}

// Status represents an issues current status. Is one of Open, In Progress,
// Done, it may have a different visual representation but those states are what
// is used internally.
type Status struct {
	ID       uint       `json:"-"`
	TicketID uint       `json:"-"`
	Name     string     `json:"name"`
	Type     StatusType `json:"type"`
}
