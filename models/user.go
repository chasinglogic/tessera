package models

import (
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// User represents a user of our application
type User struct {
	ID              uint         `json:"-" gorm:"primary_key"`
	Username        string       `json:"username" gorm:"unique"`
	Password        string       `json:"-"`
	Email           string       `json:"email" gorm:"unique"`
	FullName        string       `json:"fullName"`
	Memberships     []Membership `json:"-"`
	AssignedTickets []Ticket     `json:"assignedTickets,omitempty" gorm:"ForeignKey:Assignee"`
	ReportedTickets []Ticket     `json:"reportedTickets,omitempty" gorm:"ForeignKey:Reporter"`
	IsAdmin         bool         `json:"isAdmin"`
}

// NewUser will create the user after encrypting the password with bcrypt
func NewUser(username, password, fullName, email string, admin bool) (*User, error) {
	pw, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return &User{}, err
	}

	return &User{
		Username: username,
		Password: string(pw),
		Email:    email,
		FullName: fullName,
		IsAdmin:  admin,
	}, nil
}

func (u *User) String() string {
	return fmt.Sprintf("<User %d, %s>", u.ID, u.Username)
}

// LoginRequest represents an incoming login request, we should never send one
// of these back to the client since it contains the users password.
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Login will attempt to login the user.
func (lr *LoginRequest) Login(db *gorm.DB) (*User, error) {
	var u User
	db.Where("username = ?", lr.Username).First(&u)

	if db.Error != nil {
		return &u, db.Error
	}

	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(lr.Password))
	if err == nil {
		return &u, nil
	}

	return &u, errors.New("Incorrect password.")
}

// SignupRequest is the same as a LoginRequest however for signing up.
type SignupRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	IsAdmin  bool   `json:"isAdmin"`
}

// Signup will create the user model and save it into db
func (sr *SignupRequest) Signup(db *gorm.DB) (*User, error) {
	u, err := NewUser(
		sr.Username,
		sr.Password,
		sr.FullName,
		sr.Email,
		sr.IsAdmin,
	)

	if err != nil {
		return u, err
	}

	if db.NewRecord(&u) {
		db.Create(&u)
		return u, db.Error
	}

	return &User{}, errors.New("A user with that username / email already exists.")
}
