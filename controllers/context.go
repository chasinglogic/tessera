package controllers

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/gommon/log"
)

// GlobalContext holds items which need to be globally accessible.
// such as DB connections and loggers.
type GlobalContext struct {
	db  *gorm.DB
	log *log.Logger
}

// CheckDbError will check if there was a database error and log it if so then
// will return the error
func (gc *GlobalContext) CheckDbError() error {
	if gc.db.Error != nil {
		gc.log.Error(gc.db.Error.Error())
	}

	return gc.db.Error
}

func NewContext(db *gorm.DB, log *log.Logger) *GlobalContext {
	return &GlobalContext{
		db,
		log,
	}
}
