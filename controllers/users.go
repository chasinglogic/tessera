package controllers

import (
	"net/http"

	"github.com/chasinglogic/tessera/models"
	"github.com/labstack/echo"
)

func (gc *GlobalContext) ListUsers(c echo.Context) error {
	var users []models.User
	gc.db.Find(&users)
	if err := gc.CheckDbError(); err != nil {
		// TODO: Don't return raw terrible stuff.
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, users)
}

func (gc *GlobalContext) CreateUser(c echo.Context) error {
	var s models.SignupRequest

	c.Bind(&s)

	user, dberr := s.Signup(gc.db)
	// TODO: Handle errors properly (i.e. username taken etc.)
	if dberr != nil {
		gc.log.Error("Database error adding user", dberr)
		return c.String(http.StatusInternalServerError, dberr.Error())
	}

	return c.JSON(http.StatusOK, &user)
}

func (gc *GlobalContext) LoginUser(c echo.Context) error {
	var l models.LoginRequest

	c.Bind(&l)

	user, dberr := l.Login(gc.db)
	// TODO: Handle errors properly (i.e. bad password)
	if dberr != nil {
		gc.log.Error("Database error logging in")
		return c.String(http.StatusInternalServerError,
			"Username or password was invalid.")
	}

	return c.JSON(http.StatusOK, &user)
}

func (gc *GlobalContext) GetUser(c echo.Context) error {
	var u models.User

	// TODO: Sanitize this somehow? Not sure how this could really be used
	// maliciously....
	username := c.Param("name")

	gc.db.Where("username = ?", username).First(&u)
	if err := gc.CheckDbError(); err != nil {
		// TODO: Don't return raw terrible stuff.
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, &u)
}

// TODO: implement this, updating will suck if it needs to be performant
func (gc *GlobalContext) UpdateUser(c echo.Context) error {
	var u models.User
	c.Bind(&u)
	return nil
}
