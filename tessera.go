package main

import (
	"fmt"
	"os"

	"github.com/chasinglogic/tessera/controllers"
	"github.com/chasinglogic/tessera/utils"
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
)

func main() {
	fmt.Println("Starting Tessera!")
	fmt.Println("Initializing database...")
	db, err := utils.InitDB(os.Getenv("TESSERA_DB_URL"))
	if err != nil {
		fmt.Println("Failed to connect to database", err)
		os.Exit(1)
	}

	fmt.Println("Setting up logger...")
	logger := log.New("TSRA:")

	logger.Info("Building handlers...")
	handlers := controllers.NewContext(db, logger)

	// Config
	s := standard.New(":3000")
	e := echo.New()
	// TODO: add a e.Static here and an e.File here to serve the client
	e.Pre(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339} [${method}] |${status}| ${uri} ${latency_human}\n",
	}))

	logger.Info("Registering routes...")
	api := e.Group("/api")
	v1 := api.Group("/v1")
	{

		// tokens := v1.Group("/tokens")
		// {
		// 	tokens.POST("/:user/destroy", handlers.DestroyToken)
		// 	tokens.POST("/:user/create", handlers.CreateToken)
		// }

		users := v1.Group("/users")
		{
			users.GET("/", handlers.ListUsers)

			users.POST("/login", handlers.LoginUser)
			users.POST("/create", handlers.CreateUser)

			users.GET("/:name", handlers.GetUser)
		}

		// 			projects := v1.Group("/projects")
		// 			{
		// 				projects.GET("/", handlers.ListProjects)
		// 				projects.POST("/projects/create", handlers.CreateProject)

		// 				projects.PUT("/projects/{key}", handlers.UpdateProject)
		// 				projects.GET("/projects/{key}/members/add", handlers.AddProjectMember)
		// 				projects.GET("/projects/{key}/members", handlers.ListProjectsMembers)
		// 				projects.GET("/projects/{key}/tickets", handlers.ListProjectsTickets)
		// 			}

		tickets := v1.Group("/tickets")
		{
			tickets.POST("/", handlers.ListTickets)
			tickets.POST("/create", handlers.CreateTicket)

			tickets.GET("/:key", handlers.GetTicket)
			tickets.PUT("/:key", handlers.UpdateTicket)

			tickets.POST("/:key/advance", handlers.AdvanceTicket)
			tickets.POST("/:key/retract", handlers.RetractTicket)
		}
	}

	// logger.Info("Ready to serve requests!")
	e.Run(s)
}
